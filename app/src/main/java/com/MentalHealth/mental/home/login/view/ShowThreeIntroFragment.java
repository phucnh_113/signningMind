package com.MentalHealth.mental.home.login.view;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.MentalHealth.mental.R;
import com.MentalHealth.mental.base.BaseFragment;

public class ShowThreeIntroFragment extends BaseFragment{
    @Override
    public int getLayoutId() {
        return R.layout.fragment_slide_three;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
