package com.MentalHealth.mental.home.login.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.MentalHealth.mental.R;
import com.MentalHealth.mental.base.BaseFragment;

/**
 * Created by hoangphuc on 18/07/26.
 */
public class ShowOneIntroFragment extends BaseFragment {
    @Override
    public int getLayoutId() {
        return R.layout.fragment_slide_one;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
