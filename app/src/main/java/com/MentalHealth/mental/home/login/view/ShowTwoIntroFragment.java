package com.MentalHealth.mental.home.login.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.MentalHealth.mental.R;
import com.MentalHealth.mental.base.BaseFragment;

/**
 * Created by hoangphuc on 18/07/26.
 */
public class ShowTwoIntroFragment extends BaseFragment{
    @Override
    public int getLayoutId() {
        return R.layout.fragment_slide_two;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
