package com.MentalHealth.mental.home.model;


public class MainObject {
    private int imgDrugs;


    public MainObject(int imgDrugs) {
        this.imgDrugs = imgDrugs;
    }

    public int getImgDrugs() {
        return imgDrugs;
    }

    public void setImgDrugs(int imgDrugs) {
        this.imgDrugs = imgDrugs;
    }
}
