package com.MentalHealth.mental.constant;

import android.content.Context;

/**
 * Created by hoangphuc on 27/02/2018.
 */
public class Constants {
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String ID = "id";
    public static final String TYPE = "type";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";
    public static final String RESUME = "resume";

}