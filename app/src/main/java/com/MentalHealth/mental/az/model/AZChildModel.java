package com.MentalHealth.mental.az.model;

/**
 * Created by hoangphuc on 15/07/2018.
 */
public class AZChildModel {
    private String name;

    public AZChildModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
